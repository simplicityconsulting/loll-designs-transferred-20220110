<?php
require_once "InsynchUtil.class.php";
class InsynchOrderUpdate extends InsynchUtil{  
    public $updateQueue = array();
    /*Worker functions*/
    function getOrdersToUpdate($conn,$max = 200)
    {    
        $sql = "SELECT CustomerPONo, DATE_FORMAT(ShipExpireDate,'%m/%d/%Y') AS ShipDate
                FROM FromMas_SO_SalesOrderHeader
                INNER JOIN MasOrderHistory ON CustomerPONo = entity_id
                INNER JOIN wp_postmeta ON CustomerPONo = post_id AND meta_key = '_expected_ship_date'
                WHERE ShipExpireDate <> DATE_FORMAT(STR_TO_DATE(meta_value,'%m/%d/%Y'),'%Y%m%d') AND Processed = 0
                LIMIT $max";
        $result = mysqli_query($conn,$sql);
        $i = 0;
        if(mysqli_num_rows($result) > 0)
        {
            while($row = mysqli_fetch_assoc($result))
            {
                $this->updateQueue[$i] = $row;
                $i++;
            }
        }
        else 
        {
            $this->hPrint("No orders to update");
            return null;
        }
        return $this->updateQueue;
    }
    
    function processUpdateQueue($conn,$client)
    {   
        $updateQueue = $this->updateQueue;
        foreach ($updateQueue as $order) {
            $this->hPrint("Starting processing order: ".$order["CustomerPONo"]);
            $this->processOrder($conn,$order,$client);      
        }
    }

    function processOrder($conn,$order,$client)
    {   
        $this->updateOrder($order, $client,$conn);
        $this->markAsProcessed($conn,$order["CustomerPONo"]);
    }

    public function updateOrder($order,$client,$conn)
    {    
         $OrderID = $order["CustomerPONo"];
         $response = $client->orders->get($OrderID);
         if($response->order->status == "processing")
         {
            $ShipDate = $order["ShipDate"];
            $sql = "SELECT ShipDateComment FROM MasConfig";
            $res = mysqli_query($conn,$sql);
            $ShipMessage = "";
            if($row = mysqli_fetch_assoc($res))
            {
                $ShipMessage = str_replace('{ShipDate}',$ShipDate,$row["ShipDateComment"]);    
            } 
            $client->order_notes->create($OrderID, array("note" => "$ShipMessage","customer_note" => true)); 
            $sql = "UPDATE wp_postmeta SET meta_value = '$ShipDate' WHERE post_id = $OrderID AND meta_key = '_expected_ship_date'";
            mysqli_query($conn,$sql); 
         }
    }
    
    public function markAsProcessed($conn,$id)
    {               
        $sql = "UPDATE ".$this->insynchPrefix."FromMas_SO_SalesOrderHeader SET Processed = 1 WHERE CustomerPONo = '$id'";
        mysqli_query($conn,$sql); 
    }   
    /*End Private utility functions*/
}
?>
