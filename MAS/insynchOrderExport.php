<?php
require_once "InsynchOrderExport.class.php";
$conn = dbConnect();
$orderExport = new InsynchOrderExport();
ini_set('memory_limit', '256M');
$orderExport->processOrders($conn);
$orderExport->hPrint("Processing completed");
?>