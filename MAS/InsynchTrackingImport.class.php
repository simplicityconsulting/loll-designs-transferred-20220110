<?php
require_once "InsynchUtil.class.php";
class InsynchTrackingInventory extends InsynchUtil
{   
    public $updateQueue = array();
        
    function getInvoicesForUpdate($conn,$max = 200)
    {   
        /* change for ticket # 26119 
        $sql = "SELECT 
                invoice.InvoiceNo,
                DATE_FORMAT(invoice.ShipDate,'%Y/%m/%d') AS ShipDate,
                tracking.TrackingID AS TrackingNumber,
                shipmethod.CarrierCd AS Carrier,
                invoice.CustomerPONo AS OrderID
                FROM  ".$this->insynchPrefix."FromMas_AR_InvoiceHistoryTracking tracking
                INNER JOIN ".$this->insynchPrefix."FromMas_AR_InvoiceHistoryHeader invoice ON tracking.InvoiceNo=invoice.InvoiceNo AND CustomerPONo IN (SELECT MasSalesOrderNo FROM MasOrderHistory) 
                INNER JOIN ".$this->insynchPrefix."MasTrackingLookup shipmethod ON invoice.ShipVia = shipmethod.MasTrackingString
                WHERE tracking.Processed=0
                LIMIT $max";
        */
        $sql = "SELECT 
                invoice.InvoiceNo,
                DATE_FORMAT(invoice.ShipDate,'%Y/%m/%d') AS ShipDate,
                tracking.TrackingID AS TrackingNumber,
                shipmethod.CarrierCd AS Carrier,
                invoice.CustomerPONo AS OrderID
                FROM  ".$this->insynchPrefix."FromMas_AR_InvoiceHistoryTracking tracking
                INNER JOIN ".$this->insynchPrefix."FromMas_AR_InvoiceHistoryHeader invoice ON tracking.InvoiceNo=invoice.InvoiceNo AND CustomerPONo IN (SELECT entity_id FROM MasOrderHistory) 
                INNER JOIN ".$this->insynchPrefix."MasTrackingLookup shipmethod ON invoice.ShipVia = shipmethod.MasTrackingString
                WHERE tracking.Processed=0 AND invoice.InvoiceNo > 0066078
                LIMIT $max";
        $result = mysqli_query($conn,$sql);
        $i = 0;
        if(mysqli_num_rows($result) > 0)
        {
            while($row = mysqli_fetch_assoc($result))
            {
                $this->updateQueue[$i] = $row;
                $i++;
            }
        }
        else 
        {
            $this->hPrint("No tracking numbers to process");
            return null;
        }
        return $this->updateQueue;
    }

    function processUpdateQueue($conn,$client)
    {   
        $updateQueue = $this->updateQueue;
        foreach ($updateQueue as $invoice) {
            $this->hPrint("Starting processing invoice: ".$invoice["InvoiceNo"]);
            $this->processInvoice($conn,$invoice,$client);      
        }
    }

    function processInvoice($conn, $invoice,$client)
    {   
        if($this->updateOrder($conn,$invoice, $client))
        {
            $this->markAsProcessed($conn, $invoice["InvoiceNo"]);
        }
    }

    public function updateOrder($conn, $invoice,$client)
    {    
         try
         {
             $response = $client->orders->get($invoice["OrderID"]);
             if($response->order->status != "completed" && $response->order->status != "refunded" && $response->order->status != "cancelled" && $response->order->status != "failed")
             {   
                if($invoice["TrackingNumber"] != "")
                { 
                    $tracking = array(
                        'tracking_provider' => $invoice["Carrier"],
                        'custom_tracking_provider' => '',
                        'custom_tracking_link' => '',
                        'tracking_number' => $invoice["TrackingNumber"],
                        'date_shipped' => ''.strtotime($invoice["ShipDate"]),
                        'tracking_id' => md5("{$invoice["Carrier"]}-{$invoice["TrackingNumber"]}".microtime())
                    );
                    $trackings[0] = $tracking;
                    $tracking_items = serialize($trackings);
                    $sql = "SELECT * FROM wp_postmeta WHERE post_id = ".$invoice["OrderID"]." AND meta_key = '_wc_shipment_tracking_items'"; 
                    $result = mysqli_query($conn,$sql);
                    if(mysqli_num_rows($result) == 0)
                    {
                        $sql = "INSERT INTO wp_postmeta (post_id,meta_key,meta_value) VALUES (".$invoice["OrderID"].",'_wc_shipment_tracking_items','$tracking_items')";
                        $result = mysqli_query($conn,$sql); 
                    }
                }
                $updated_order = $client->orders->update_status($invoice["OrderID"],"completed");    
                if($updated_order != null)
                    return true;
             }
             else if($response->order->status == "completed")
             {
                 return true;
             }
         } 
         catch(Exception $e)
         {  
            return true;
         }
         return false;
    }
    
    public function markAsProcessed($conn,$InvoiceNo)
    {               
        $sql = "UPDATE ".$this->insynchPrefix."FromMas_AR_InvoiceHistoryTracking SET Processed = 1 WHERE InvoiceNo = '$InvoiceNo'";
        mysqli_query($conn,$sql); 
    }   
}                                                        
?>