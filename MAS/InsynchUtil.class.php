<?php
require_once( 'lib/woocommerce-api.php' );
require_once "includes/conn.inc.php"; 
$conn = dbConnect();

class InsynchUtil
{   
    var $emailNotificationList ='patricia@simplicityc.com';
    var $insynchPrefix='';
    var $storeURL = 'https://lolldesigns.com/';
    var $consumerKey = 'ck_cad00c3a1c16b848265f9419f1cb22e44cad6896';
    var $consumerSecret = 'cs_1cc509b1662322773dfdb0f9f095163d46d9926c';
    var $options = array(
                'debug'           => true,
                'return_as_array' => false,
                'validate_url'    => false,
                'timeout'         => 30,
                'ssl_verify'      => false,
            );
    
    function hPrint($text) {
        echo(microtime(true));
        echo ("&nbsp;$text<br/>");
    }
    
    function sendErrorEmail($text)
    {    
        $subject = "Error in Woo Commerce MAS Integration";
        $message = "$text";
        $headers = 'From: website@simplicityc.com' . "\r\n" .
        'Reply-To: website@simplicityc.com' . "\r\n" .
        'X-Mailer: PHP/' . phpversion();
        if(mail($this->emailNotificationList, $subject, $message, $headers))
        {
            $trackingImport->hPrint("Error email was sent");
        }
        else
        {
            $trackingImport->hPrint("Error email couldn't be sent");
        }     
    }
    
    function sendErrorNotificationEmail($conn, $entityType, $entityID, $text)
    {
        $sql =    "SELECT COUNT(*) AS TotalMessages
        FROM ".$this->insynchPrefix."MasErrorNotificationLog
        WHERE EntityType='$entityType' AND EntityID = $entityID";
        $result = mysqli_query($conn,$sql);
        $row = mysqli_fetch_assoc($result);
        if ($row["TotalMessages"]==0)
        {
            $qtext=addslashes($text);

            $sql="INSERT INTO ".$this->insynchPrefix."MasErrorNotificationLog(EntityType, EntityID, RecipientList, MessageText,DateReported)
                  VALUES ('$entityType','$entityID','".$this->emailNotificationList."','$qtext', '".date('Y-m-d-h:i:s')."')";
            mysqli_query($sql);
            $this->sendErrorEmail($text);
        }
        else
        {
            $this->hPrint("Error was documented before");
        }
    }
}
?>
