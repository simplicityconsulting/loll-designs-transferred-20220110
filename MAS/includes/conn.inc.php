<?php
function dbConnect() {
    if (isset($_ENV['PANTHEON_ENVIRONMENT']))
    {
        $server=$_ENV['DB_HOST'];
        $user=$_ENV['DB_USER'];
        $pwd=$_ENV['DB_PASSWORD'];
        $db=$_ENV['DB_NAME'];
        $port=$_ENV['DB_PORT'];
    }
    else
    {
        $server="dbserver.live.42922010-8834-4327-ba00-84e6f2885744.drush.in";
        $user="pantheon";
        $pwd="b4bf56a3395a445789b3524caf52c411";
        $db="pantheon";
        $port="11445";
    }
    $conn = mysqli_connect($server, $user, $pwd, $db, $port) or die ('Cannot connect to server');    
    mysqli_select_db($conn,$db) or die ('Cannot open database');
    return $conn;    
}
?>
