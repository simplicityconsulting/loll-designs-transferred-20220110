<?php
require_once "InsynchUtil.class.php";
class InsynchOrderExport extends InsynchUtil{  
    /*Worker functions*/
    public $changeCustomer = false;
    function processOrders($conn) {
        try
        {   
            $client = new WC_API_Client($this->storeURL, $this->consumerKey, $this->consumerSecret, $this->options);
            $morePages = true;
            $page = 0;
            while($morePages == true)
            {
                $page++;
                $response = $client->orders->get(null, array( 'status' => 'processing','page'=>$page,'orderby' => 'date','filter[orderby]' => 'date','filter[order]' => 'ASC','filter[created_at_min]' => date('Y-m-d',strtotime("-7 days"))." 00:00:00"));
                if($response->orders == null)
                {
                    $morePages = false;
                }
                foreach($response->orders as $order)
                {
                    if(!$this->OrderWasProcessed($conn, $order->id))
                    {
                        $this->hPrint("Starting processing on order: ".$order->id."");
                        $this->processOrder($conn, $order);      
                    }
                }
            }
        }
        catch ( WC_API_Client_Exception $e ) 
        {
            echo $e->getMessage() . PHP_EOL;
            echo $e->getCode() . PHP_EOL;
            if ( $e instanceof WC_API_Client_HTTP_Exception ) 
            {
                print_r( $e->get_request() );
                print_r( $e->get_response() );
            }
        }
    }
    
    function processOrder($conn, $order) {
        $MasSalesOrderNo = "I".$order->id;
        $this->preOrderCleanup($conn, $MasSalesOrderNo);
        $this->UseWooShipDate = false;
        $this->changeCustomer = false;
        if(!$this->processOrderItems($conn, $order, $MasSalesOrderNo))
        {
            $this->hPrint("Error processing order items");
            $this->sendErrorNotificationEmail("Item", $orderId, "Error processing items for order number  ".$order->id." \r\n");
            return false;
        }
        if(!$this->processOrderHeader($conn, $order, $MasSalesOrderNo))
        {
            $this->hPrint("Error processing order header");
            $this->sendErrorNotificationEmail("Order", $orderId, "Error processing order header for order ".$order->id." \r\n");
            return false;
        }
        $this->markOrderAsProcessed($conn, $order->id, $MasSalesOrderNo);
        $this->hPrint("Proccessing completed for this order");
        return true;
    }
             
    function markOrderAsProcessed($conn, $orderId, $MasSalesOrderNo) {
        $sql = "INSERT INTO ".$this->insynchPrefix."MasOrderHistory (entity_id, MasSalesOrderNo) values ($orderId ,'$MasSalesOrderNo')";
        return (mysqli_query($conn,$sql));
    }                           
    /*End Worker functions*/

    /*Database writing functions*/
    function preOrderCleanup($conn, $MasSalesOrderNo) {
        $sql= "DELETE FROM ".$this->insynchPrefix."ToMas_SO_SalesOrderDetail WHERE SalesOrderNo='$MasSalesOrderNo'";
        $sql2="DELETE FROM ".$this->insynchPrefix."ToMas_SO_SalesOrderHeader WHERE SalesOrderNo='$MasSalesOrderNo'";
        mysqli_query($conn,$sql);
        mysqli_query($conn,$sql2);
    }
          
    function processOrderItems($conn, $order, $MasSalesOrderNo) {
        foreach($order->line_items as $item)
        {   
            $sql = "INSERT INTO ".$this->insynchPrefix."ToMas_SO_SalesOrderDetail
               (SalesOrderNo
               ,SequenceNo
               ,ItemCode
               ,ItemType
               ,ItemCodeDesc      
               ,WarehouseCode          
               ,QuantityOrderedOriginal
               ,OriginalUnitPrice)
               VALUES
               ('$MasSalesOrderNo',
               '".$item->id."',
               '".$item->sku."',
               '1',
               '".addslashes($item->name)."',
               '000',
               ".$item->quantity.",
               ".$item->price.")";
            $sqlLog = str_replace("ToMas_SO_SalesOrderDetail","ToMas_SO_SalesOrderDetailLog", $sql);
            mysqli_query($conn,$sqlLog);
            mysqli_query($conn,$sql);
            
            if(substr($item->sku,0,2) == "TT"){
                $this->changeCustomer = true;
            }
            
            $sql = "SELECT * FROM FromMas_IM1_InventoryMasterfile WHERE ItemNumber = '".$item->sku."' AND ProductLine NOT IN (SELECT ProductLine FROM MasProductLineLookup)";
            $res = mysqli_query($conn,$sql);
            if(mysqli_num_rows($res) > 0 || substr($item->sku,0,1) != "Y")
            {
                $this->UseWooShipDate = true;
            }
        }
        return true;
    }
    
    function processOrderHeader($conn, $order, $MasSalesOrderNo) {  
        //Getting customer number and division
        $sqlCustomer = "SELECT CustomerNo, ArDivisionNo
                        FROM ".$this->insynchPrefix."FromMas_AR_Customer 
                        WHERE EmailAddress = '".$order->billing_address->email."'
                        LIMIT 1";
        $result = mysqli_query($conn,$sqlCustomer);
        $CustomerNo = "";  
        $ArDivisionNo = "00";
        if($row = mysqli_fetch_assoc($result))
        {
            $CustomerNo = $row["CustomerNo"];
            $ArDivisionNo = $row["ArDivisionNo"];
        }
        if($this->changeCustomer){
            $CustomerNo = "0008580";  
            $ArDivisionNo = "00";    
        }
        $postcode = $order->shipping_address->postcode;
        $postcode = substr($postcode, 0, 5);
        $state = $order->shipping_address->state;
        $country = $order->shipping_address->country;
        /*if($country == 'US'){
            if($state == 'WA'){
                $TaxSchedule = 'WA';
            }
            if($state == 'MN'){
                $sqlMinnesota = "SELECT TaxCode FROM MasTaxSchedule WHERE PostCode = '$postcode'";
                $result = mysqli_query($conn,$sqlMinnesota);
                if($row = mysqli_fetch_assoc($result)){
                    $TaxSchedule = $row["TaxCode"];
                }
                else{
                    $TaxSchedule = 'MN';
                }
            }    
        }
        if($country == 'CA'){
            if($state == 'ON'){
                $TaxSchedule = 'ONT-CA';
            } else{
                $TaxSchedule = 'GST';
            }
        }*/
        /* change per ticket#26125
        if($order->tax_lines != null)
        {
            $TaxSchedule = $order->tax_lines[0]->title;
        }
        else
        {
            $TaxSchedule = 'NONTAX';
        } */
        $TaxSchedule = "NONTAX";
        if($order->total_tax > 0)
        {
            $TaxSchedule = "AVATAX";
        }
          
        if($this->UseWooShipDate == true)
        {
            $sql = "SELECT DATE_FORMAT(STR_TO_DATE(meta_value,'%m/%d/%Y'),'%Y%m%d') AS ShipDate FROM wp_postmeta WHERE meta_key = '_expected_ship_date' AND post_id = '".$order->id."'";
            $result = mysqli_query($conn,$sql);
            if($row = mysqli_fetch_assoc($result))
            {
                $this->ShipDate = $row["ShipDate"];    
            }
        }       
        else
        {
            $this->ShipDate = date ('Ymd',strtotime('4 weeks'));    
        }
        
        $sql = "SELECT meta_value AS auth_code FROM wp_postmeta WHERE meta_key = '_wc_authorize_net_cim_credit_card_authorization_code' AND post_id = '".$order->id."'";
        $result = mysqli_query($conn,$sql);
        $AuthCode = "";
        if($row = mysqli_fetch_assoc($result))
        {
            $AuthCode = $row["auth_code"];    
        }
            
        $sql = "INSERT INTO ".$this->insynchPrefix."ToMas_SO_SalesOrderHeader
               (SalesOrderNo
               ,OrderDate
               ,OrderStatus
               ,MasterRepeatingOrderNo
               ,ARDivisionNo
               ,CustomerNo
               ,BillToName
               ,BillToAddress1
               ,BillToAddress2
               ,BillToAddress3
               ,BillToCity
               ,BillToState
               ,BillToZipCode
               ,BillToCountryCode
               ,ShipToCode
               ,ShipToName
               ,ShipToAddress1
               ,ShipToAddress2
               ,ShipToAddress3
               ,ShipToCity
               ,ShipToState
               ,ShipToZipCode
               ,ShipToCountryCode
               ,ShipVia
               ,ShipZone
               ,ShipZoneActual
               ,ShipWeight
               ,CustomerPONo
               ,EmailAddress
               ,ResidentialAddress
               ,CancelReasonCode
               ,FreightCalculationMethod
               ,FOB
               ,WarehouseCode
               ,ConfirmTo
               ,Comment
               ,TaxSchedule
               ,TermsCode
               ,TaxExemptNo
               ,RMANo
               ,JobNo
               ,LastInvoiceDate
               ,LastInvoiceNo
               ,CheckNoForDeposit
               ,LotSerialLinesExist
               ,SalespersonDivisionNo
               ,SalespersonNo
               ,SplitCommissions
               ,SalespersonDivisionNo2
               ,SalespersonNo2
               ,SalespersonDivisionNo3
               ,SalespersonNo3
               ,SalespersonDivisionNo4
               ,SalespersonNo4
               ,SalespersonDivisionNo5
               ,SalespersonNo5
               ,EBMUserType
               ,EBMSubmissionType
               ,EBMUserIDSubmittingThisOrder
               ,PaymentType
               ,OtherPaymentTypeRefNo
               ,CorporateCustIDPurchOrder
               ,CorporateTaxOverrd
               ,DepositCorporateTaxOverrd
               ,CardholderName
               ,ExpirationDateYear
               ,ExpirationDateMonth
               ,EncryptedCreditCardNo
               ,Last4UnencryptedCreditCardNos
               ,CreditCardAuthorizationNo
               ,CreditCardTransactionID
               ,AuthorizationDate
               ,AuthorizationTime
               ,AuthorizationCodeForDeposit
               ,CreditCardTransactionIDForDep
               ,PaymentTypeCategory
               ,PayBalanceByCreditCard
               ,FaxNo
               ,CRMUserID
               ,CRMCompanyID
               ,CRMPersonID
               ,CRMOpportunityID
               ,TaxableSubjectToDiscount
               ,NonTaxableSubjectToDiscount
               ,TaxSubjToDiscPrcntTotSubjTo
               ,DiscountRate
               ,DiscountAmt
               ,TaxableAmt
               ,NonTaxableAmt
               ,SalesTaxAmt
               ,CreditCardPreAuthorizationAmt
               ,CommissionRate
               ,SplitCommRate2
               ,SplitCommRate3
               ,SplitCommRate4
               ,SplitCommRate5
               ,Weight
               ,FreightAmt
               ,DepositAmt
               ,CreditCardPaymentBalanceAmt
               ,DepositCorporateSalesTax
               ,CorporateSalesTax
               ,DateCreated
               ,TimeCreated
               ,UserCreatedKey
               ,DateUpdated
               ,TimeUpdated
               ,UserUpdatedKey
               ,ShipToPhone
               ,BillToPhone
               ,WebOrderNumber
               ,UDF_CONTACT
               ,UDF_PHONE
               ,UDF_NOTE
               ,ShipDate)
               SELECT 
               '$MasSalesOrderNo' AS SalesOrderNo,
               '".date('Ymd',strtotime($order->created_at))."' AS OrderDate,
               Null AS OrderStatus,
               Null AS MasterRepeatingOrderNo,
               '$ArDivisionNo' AS ARDivisionNo,
               '$CustomerNo' AS CustomerNo,
               CONCAT('".addslashes($order->billing_address->first_name)."',' ','".addslashes($order->billing_address->last_name)."') AS BillToName,
               '".addslashes($order->billing_address->address_1)."' AS BillToAddress1,
               '".addslashes($order->billing_address->address_2)."' AS BillToAddress2,
               '".addslashes($order->billing_address->company)."' AS BillToAddress3,
               '".addslashes($order->billing_address->city)."' AS BillToCity,
               '".$order->billing_address->state."' AS BillToState,
               '".$order->billing_address->postcode."' AS BillToZipCode,
               '".$order->billing_address->country."' AS BillToCountryCode,
               Null AS ShipToCode,
               CONCAT('".addslashes($order->shipping_address->first_name)."',' ','".addslashes($order->shipping_address->last_name)."') AS ShipToName,
               '".addslashes($order->shipping_address->address_1)."' AS ShipToAddress1,
               '".addslashes($order->shipping_address->address_2)."' AS ShipToAddress2,
               '".addslashes($order->shipping_address->company)."' AS ShipToAddress3,
               '".addslashes($order->shipping_address->city)."' AS ShipToCity,
               '".$order->shipping_address->state."' AS ShipToState,
               '".$order->shipping_address->postcode."' AS ShipToZipCode,
               '".$order->shipping_address->country."' AS ShipToCountryCode,
               (SELECT MasShipMethod FROM ".$this->insynchPrefix."MasShippingLookup WHERE ShipMethod = '".$order->shipping_methods."') AS ShipVia,
               Null AS ShipZone,
               Null AS ShipZoneActual,
               Null AS ShipWeight,
               '".$order->id."' AS CustomerPONo, 
               '".$order->billing_address->email."' AS EmailAddress,
               Null AS ResidentialAddress,
               Null AS CancelReasonCode,
               Null AS FreightCalculationMethod,
               Null AS FOB,
               '000' AS WarehouseCode,
               CONCAT('".addslashes($order->billing_address->first_name)."',' ','".addslashes($order->billing_address->last_name)."') AS ConfirmTo,
               '".addslashes($order->note)."' AS Comment,
               '$TaxSchedule' AS TaxSchedule,
               (SELECT MasTermsCode FROM ".$this->insynchPrefix."MasPaymentLookup WHERE PaymentMethod = '".$order->payment_details->method_id."') AS TermsCode,
               Null AS TaxExemptNo,
               Null AS RMANo,
               Null AS JobNo,
               Null AS LastInvoiceDate,
               Null AS LastInvoiceNo,
               '$AuthCode' AS CheckNoForDeposit,
               Null AS LotSerialLinesExist,
               '00' AS SalespersonDivisionNo,
               'IT' AS SalespersonNo,
               Null AS SplitCommissions,
               Null AS SalespersonDivisionNo2,
               Null AS SalespersonNo2,
               Null AS SalespersonDivisionNo3,
               Null AS SalespersonNo3,
               Null AS SalespersonDivisionNo4,
               Null AS SalespersonNo4,
               Null AS SalespersonDivisionNo5,
               Null AS SalespersonNo5,
               Null AS EBMUserType,
               Null AS EBMSubmissionType,
               Null AS EBMUserIDSubmittingThisOrder,
               (SELECT MasPaymentMethod FROM ".$this->insynchPrefix."MasPaymentLookup WHERE PaymentMethod = '".$order->payment_details->method_id."') AS PaymentType,
               Null AS OtherPaymentTypeRefNo,
               Null AS CorporateCustIDPurchOrder,
               Null AS CorporateTaxOverrd,
               Null AS DepositCorporateTaxOverrd,
               Null AS CardholderName,
               Null AS ExpirationDateYear,
               Null AS ExpirationDateMonth,
               Null AS EncryptedCreditCardNo,
               Null AS Last4UnencryptedCreditCardNos,
               Null AS CreditCardAuthorizationNo,
               Null AS CreditCardTransactionID,
               Null AS AuthorizationDate,
               Null AS AuthorizationTime,
               Null AS AuthorizationCodeForDeposit,
               Null AS CreditCardTransactionIDForDep,
               Null AS PaymentTypeCategory,
               Null AS PayBalanceByCreditCard,
               Null AS FaxNo,
               Null AS CRMUserID,
               Null AS CRMCompanyID,
               Null AS CRMPersonID,
               Null AS CRMOpportunityID,
               Null AS TaxableSubjectToDiscount,
               Null AS NonTaxableSubjectToDiscount,
               Null AS TaxSubjToDiscPrcntTotSubjTo,
               Null AS DiscountRate,
               Null AS DiscountAmt,
               Null AS TaxableAmt,
               Null AS NonTaxableAmt,
               Null AS SalesTaxAmt,
               Null AS CreditCardPreAuthorizationAmt,
               Null AS CommissionRate,
               Null AS SplitCommRate2,
               Null AS SplitCommRate3,
               Null AS SplitCommRate4,
               Null AS SplitCommRate5,
               Null AS Weight,
               ".$order->total_shipping." AS FreightAmt,
               ".$order->total." AS DepositAmt,
               Null AS CreditCardPaymentBalanceAmt,
               Null AS DepositCorporateSalesTax,
               Null AS CorporateSalesTax,
               Null AS DateCreated,
               Null AS TimeCreated,
               Null AS UserCreatedKey,
               Null AS DateUpdated,
               Null AS TimeUpdated,
               Null AS UserUpdatedKey,
               Null AS ShipToPhone,
               Null AS BillToPhone,
               '".$order->id."' AS WebOrderNumber,
               CONCAT('".addslashes($order->billing_address->first_name)."',' ','".addslashes($order->billing_address->last_name)."') AS UDF_CONTACT,
               '".$order->billing_address->phone."' AS UDF_PHONE,
               '".$order->note."' AS UDF_NOTE,
               '".$this->ShipDate."' AS ShipDate";
        $sqlLog = str_replace("ToMas_SO_SalesOrderHeader","ToMas_SO_SalesOrderHeaderLog", $sql);
        mysqli_query($conn, $sqlLog);
        return mysqli_query($conn, $sql);
    }      
    /*End Database Writing Functions*/

    /*Private utility functions*/
    function generateMasSalesOrderNo($id) {
        $prefix='WC';
        $result = str_pad(( int ) $id, 7-strlen($prefix), "0", STR_PAD_LEFT);
        $result = "$prefix$result";
        return $result;
    } 
    
    function OrderWasProcessed($conn,$id) {
        $sql = "SELECT * FROM ".$this->insynchPrefix."MasOrderHistory WHERE entity_id = $id";
        $results = mysqli_query($conn, $sql);
        return mysqli_num_rows($results);
    } 
    /*End Private utility functions*/
}
?>
